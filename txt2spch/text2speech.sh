##!/usr/bin/env bash
echo "Welcome to Bash! This is text to speech"

echo "Executing text to texttospeech."

echo "Getting translated input"

inputTXT=`cat txtinput.json | python -m json.tool | grep audioContent | cut -f4 -d '"'`

txt2speech=`curl -H "Authorization: Bearer $(gcloud auth application-default print-access-token)" \
  -H "Content-Type: application/json; charset=utf-8" \
  --data "{
    \"input\":{
      \"text\":\"$inputTXT\"
    },
    \"voice\":{
      \"languageCode\":\"hi-IN\",
      \"name\":\"hi-IN-Wavenet-A\",
      \"ssmlGender\":\"FEMALE\"
    },
    \"audioConfig\":{
      \"audioEncoding\":\"MP3\"
    }
  }" "https://texttospeech.googleapis.com/v1/text:synthesize" | python -m json.tool | grep audioContent | cut -f4 -d'"'`

echo "Decoding speech!"

echo $txt2speech > decodedTxt2Spch.txt

echo "Converting to Audio File"

base64 decodedTxt2Spch.txt -D > HSBC-Premier.mp3

echo "Playing back audio"

afplay ./HSBC-Premier.mp3 &

## intent:affirm
- yes
- indeed
- of course
- that sounds good
- correct

## intent:deny
- no
- never
- I don't think so
- don't like that
- no way
- not really

## intent:goodbye
- bye
- goodbye
- see you around
- see you later
- bye

## intent:greet
- hey
- hello
- hi
- good morning
- good evening
- hey there
- hello

## intent:mood_great
- perfect
- very good
- great
- amazing
- wonderful
- I am feeling very good
- I am great
- I'm good
- good

## intent:mood_unhappy
- sad
- very sad
- unhappy
- bad
- very bad
- awful
- terrible
- not very good
- extremely sad
- so sad

## intent:appointment
- need an appointment
- looking for an appointment
- can i get an appointment
- account appointment
- RM appointment
- relationship manager

## intent:new
- new
- fresh
- yes

## intent:choice
- today
- current date
- today's date

##intent: change
- existing
- current

##intent: cancel
- existing
- no longer
- not required
- current

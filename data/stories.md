## happy path
* greet
  - utter_greet
* mood_great
  - utter_happy_customer

## appointment
* appointment
  - utter_ask_details

## new
* new
  - utter_new_appointment

## choice
* choice
- utter_choice_confirmation

## sad path
* greet
  - utter_greet
* mood_unhappy
  - utter_unhappy_customer
  #- utter_ask_details
* deny
  - utter_goodbye

## say goodbye
* goodbye
  - utter_goodbye

## interactive_story_1
* greet
    - utter_greet
* mood_great
    - utter_happy_customer
* goodbye
    - utter_goodbye
